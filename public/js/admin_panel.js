$(document).ready(function(){
    $(".button-collapse").sideNav();
    $('select').material_select();
    $('.materialboxed').materialbox();

});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function deleteAll(route) {

    $.ajax({
        url: route,
        type: 'DELETE',
        dataType: 'json',
        success: function (response) {
            $('#' + response.id).remove();
            location.reload();
        },
        error: function () {
            alert('ERROR')
        }
    });
}