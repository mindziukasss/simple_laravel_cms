<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pages';

    protected $fillable = ['user_id','category_id','title','description','slug','visible','cover','position'];


    public function user(){
        return $this->hasMany(User::class,'id','user_id');
    }


}
