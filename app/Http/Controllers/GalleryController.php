<?php

namespace App\Http\Controllers;

use App\Gallery;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class GalleryController extends Controller
{

    public function upload(UploadedFile $file)
    {
        $data =
            [
                "size" => $file->getsize(),
                "mime_type" => $file->getMimetype(),
            ];
        $path = '/upload/' . date("Y/m/d/");
        $fileName = Carbon::now()->timestamp . '-' . $file->getClientOriginalName();
        $file->move(public_path($path), $fileName);
        $data["path"] = $path . $fileName;

        return Gallery::create($data);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {
        $galleries = Gallery::all();
        return view('admin.gallery.index')->with(['galleries' => $galleries]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (isset($request['files'])) {
            if ($request['files'] != null) {
                $resourcesId = [];
                foreach ($request['files'] as $resource) {
                    $record = $this->upload($resource, null);
                    $resourcesId[] = $record->id;
                }

            }
        }
        return redirect(route('image.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gallery::destroy($id);
        return ["success" => true, "id" => $id];
    }
}
