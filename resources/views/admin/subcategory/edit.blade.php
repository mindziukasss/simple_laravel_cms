@extends('admin.core')

@section('content')

    <div class="row">


        {!! Form::open(['url' =>  route('subcategory.update',['id'=>$subCategory->id]), 'method' => 'POST', 'class'=>'col s12'])!!}
        <input type="hidden" name="_method" value="PUT">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
            <div class="input-field col s6">

                {{Form::label('name', 'Category name')}}
                {{Form::text('name', $subCategory->name)}}

            </div>
        </div>

        <div class="row">
            <div class="input-field col s6">

                {{Form::label('position', 'Enter subcategory position')}}
                {{Form::text('position', $subCategory->position)}}

            </div>
        </div>

        <div class="row">
            <div class="input-field col s6">


                {{Form::select('category_id',$category , $subCategory->position)}}
            </div>
        </div>


        {{Form::submit('Submit', array('class' => 'btn waves-effect waves-light')) }}

        {!! Form::close() !!}
    </div>










@endsection