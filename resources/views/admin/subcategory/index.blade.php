

@extends('admin.core')

@section('content')
    <h2>Category table</h2>

    <a href="{{route('subcategory.create')}}">
        <button type="button" class="waves-effect waves-light yellow btn">New category</button>
    </a>

    <table class="striped">
        <thead>
        <tr>
            <th>id</th>
            <th>position</th>
            <th>name</th>
            <th>category id</th>
        </tr>
        </thead>

        <tbody>

        @foreach ($list as $key => $value)
            <tr id="{{$value->id}}">
                <td>{{$value->id}}</td>
                <td>{{$value->position}}</td>
                <td>{{$value->name}}</td>
                <td>{{$value->category_id}}</td>

                <td>
                    <a href="{{route('subcategory.edit',$value->id)}}">
                        <button type="button" class="waves-effect waves-light btn">Edit</button>
                    </a>
                </td>

                <td>
                    <a>
                        <button type="button" class="waves-effect  waves-light red btn"
                                onclick="deleteAll('{{route('subcategory.destroy',$value->id)}}')">Delete
                        </button>
                    </a>
                </td>


                @endforeach
            </tr>
        </tbody>
    </table>
@endsection

