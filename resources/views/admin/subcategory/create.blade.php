@extends('admin.core')

@section('content')

    <div class="row">


        {!! Form::open(['url' =>  route('subcategory.store'), 'method' => 'post', 'class'=>'col s12'])!!}

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
            <div class="input-field col s6">

                {{Form::label('name', 'SubCategory name')}}
                {{Form::text('name', null)}}

            </div>
        </div>

        <div class="row">
            <div class="input-field col s6">

                {{Form::label('position', 'Enter subcategory position')}}
                {{Form::text('position', null)}}

            </div>
        </div>

        <div class="row">
            <div class="input-field col s6">


                {{Form::select('category_id',$category , null, ['placeholder' => 'Pick a category id...'])}}
            </div>
        </div>


        {{Form::submit('Submit', array('class' => 'btn waves-effect waves-light')) }}

        {!! Form::close() !!}
    </div>










@endsection