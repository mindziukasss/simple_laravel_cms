
@extends('admin.core')
@section('content')
<h2>Gallery table</h2>
<form action="{{ route('image.store') }}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="file-field input-field">
        <div class="btn">
            <span>New image or gallery</span>
            <input class="file-path validate" type="file" multiple onchange="this.form.submit()" name="files[]">
        </div>
        <div class="file-path-wrapper">
            <input class="file-path validate">
        </div>
    </div>
</form>
@foreach($galleries as $key => $gallery)
<div class="row">
    <div class="col s12 m3">
        <div class="card">
            <div class="card-image">
                <img class="materialboxed" width="650" src="{{$gallery->path}}">
            </div>
            <div class="card-content">
                <a onclick="deleteAll('{{route('image.destroy', $gallery->id)}}')" class="waves-effect waves-light">Delete</a>
            </div>
        </div>
    </div>
@endforeach
</div>
@endsection

