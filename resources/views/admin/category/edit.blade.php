@extends('admin.core')

@section('content')

    <div class="row">


            {!! Form::open(['url' =>  route('category.edit',$category->id), 'method' => 'post', 'class'=>'col s12'])!!}

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="input-field col s6">

                    {{Form::label('name', 'Category name')}}
                    {{Form::text('name', $category->name)}}

                </div>
            </div>

            <div class="row">
                <div class="input-field col s6">

                    {{Form::label('position', 'Enter category position')}}
                    {{Form::text('position', $category->position)}}

                </div>
            </div>



            {{Form::submit('Submit', array('class' => 'btn waves-effect waves-light')) }}

            {!! Form::close() !!}
    </div>










@endsection