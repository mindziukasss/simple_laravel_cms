@extends('admin.core')

@section('content')

    <div class="row">


            {!! Form::open(['url' =>  route('category.create'), 'method' => 'post', 'class'=>'col s12'])!!}

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="input-field col s6">

                    {{Form::label('name', 'Category name')}}
                    {{Form::text('name', null)}}

                </div>
            </div>

            <div class="row">
                <div class="input-field col s6">

                    {{Form::label('position', 'Enter category position')}}
                    {{Form::text('position', null)}}

                </div>
            </div>



            {{Form::submit('Submit', array('class' => 'btn waves-effect waves-light')) }}

            {!! Form::close() !!}
    </div>










@endsection