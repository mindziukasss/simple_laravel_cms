@extends('admin.core')

@section('content')
    <div class="row">
        <h4>Create page</h4>
        <form class="col s12" action="{{ route('page.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
                <div class="input-field col s12 m6">
                    <select name="category">
                        <option value="1">Category 1</option>
                    </select>
                    <label>Category</label>
                </div>
                <div class="input-field col s12 m6">
                    <select name="subcategory">
                        <option value="" disabled selected>Choose page subcategory</option>
                        <option value="1">SubCategory 1</option>
                        <option value="1">SubCategory 2</option>
                    </select>
                    <label>Sub Category</label>
                </div>
                <div class="input-field col s12 m6">
                    <select name="position">
                        <option value="1">Position 1</option>
                    </select>
                    <label>Position</label>
                </div>
                <div class="input-field col s12 m6">
                    <select icons name="image[]" id="image" multiple>
                        <option value="" disabled selected>Choose images</option>
                        @foreach($galleries as $cover)
                            <option value="{{$cover->path}}" data-icon="{{$cover->path}}" class="left circle"></option>
                        @endforeach
                    </select>
                    <label>Choose image</label>
                </div>
                <div class="input-field col s12">
                    <input name="title" id="title" type="text" class="validate">
                    <label for="title">Title</label>
                </div>
                <div class="input-field col s12">
                    <label>Textarea</label>
                    <br>
                    <br>
                    <textarea name="description"></textarea>
                </div>
            </div>
            <button class="btn waves-effect waves-light" type="submit">Save Page</button>
        </form>
        @include('mceImageUpload::upload_form')
    </div>

@endsection

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="/js/tinymce/js/tinymce.min.js"></script>
<script src="/js/tinymce.js"></script>