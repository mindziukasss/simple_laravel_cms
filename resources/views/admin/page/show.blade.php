@extends('admin.core')


@section('content')
    <div class="row">
        <div class="col s12 m10 offset-m1">
            <div class="card">
                <div class="card-image waves-effect waves-block waves-light">
                    <img class="activator" id="title_cover" src="/images/no-image.jpg"> {{--must be {{$page->cover}}--}}
                </div>
                <div class="card-panel">
                    <h4>Title: {{$page->title}}</h4>
                    <p>Category: {{$page->category_id}}</p>
                    <p>{{$page->description}}</p>
                    <p>Position: {{$page->position}}</p>
                    <p>Slug: {{$page->slug}}</p>
                    <p>Create: {{$page->created_at->format('Y-m-d')}}</p>
                    <p>
                        <a href="{{route('page.edit', $page->id)}}" class="waves-effect waves-light btn">Edit</a>
                        <a class="waves-effect waves-light btn">Off</a>{{-- must be {{$page->visible}}--}}
                        <a class="waves-effect waves-light btn" href="{{route('page.index')}}">Back</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection