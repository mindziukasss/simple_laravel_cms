@extends('admin.core')

@section('content')
    <div class="row">
        <h4>Edit page {{$page->title}}</h4>
        <form class="col s12" action="{{ route('page.update', ['id' => $page->id]) }}" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="input-field col s12 m6">
                    <select>
                        <option value="1">Category 1</option>
                    </select>
                    <label>Category</label>
                </div>
                <div class="input-field col s12 m6">
                    <select>
                        <option value="" disabled selected>Choose page subcategory</option>
                        <option value="1">SubCategory 1</option>
                    </select>
                    <label>Sub Category</label>
                </div>
                <div class="input-field col s12 m6">
                    <select>
                        <option value="1">Position 1</option>
                    </select>
                    <label>Position</label>
                </div>
                <div class="input-field col s12 m6">
                    <select>
                        <option value="" disabled selected>Choose page image</option>
                        <option value="1">Cover image</option>
                    </select>
                    <label>Cover image</label>
                </div>
                <div class="input-field col s12">
                    <input id="title" type="text" class="validate">
                    <label for="title">Title</label>
                </div>
                <div class="input-field col s12">
                    <label>Textarea</label>
                    <br>
                    <br>
                    <textarea></textarea>
                </div>
            </div>
            <button class="btn waves-effect waves-light" type="submit" name="action">Save Page</button>
        </form>
        @include('mceImageUpload::upload_form')
    </div>

@endsection

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="/js/tinymce/js/tinymce.min.js"></script>
<script src="/js/tinymce.js"></script>