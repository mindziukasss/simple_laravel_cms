@extends('admin.core')


@section('content')
<div class="row">
    <h2>All pages</h2>
    <a class="waves-effect waves-light btn" href="{{route('page.create')}}">New page</a>
</div>
<div class="row">
    @foreach($pages as $key => $page)
    <div class="col s12 m6">
        <div class="card small">
            <div class="card-image waves-effect waves-block waves-light">
                <img class="activator" id="title_cover" src="/images/no-image.jpg"> {{--must be {{$page->cover}}--}}
            </div>
            <div class="card-content">
                <span class="card-title activator grey-text text-darken-4">{{$page->title}}<i class="material-icons right">more_vert</i></span>
                <p><a href="{{route('page.edit', $page->id)}}" class="waves-effect waves-light btn">Edit</a>
                {{--<a href="{{route('page.destroy', $page->id)}}" class="waves-effect waves-light btn">Delete</a>--}}

                    <button onclick="deleteAll('{{route('page.destroy', $page->id)}}')" class="waves-effect waves-light btn">Delete</button>


                <a class="waves-effect waves-light btn">Off</a></p> {{--must be {{$page->visible}}--}}
            </div>
            <div class="card-reveal">
                <span class="card-title grey-text text-darken-4">{{$page->title}}<i class="material-icons right">close</i></span>
                <p>Category: {{$page->category_id}}</p>
                <p>{{substr(strip_tags($page->description),0, 500)}}{{ strlen(strip_tags($page->description)) > 500 ? '...' : "" }}
                    <a href="{{route('page.show', $page->id)}}">...more</a></p>
                <p>Position: {{$page->position}}</p>
                <p>Create: {{$page->created_at->format('Y-m-d')}}</p>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection