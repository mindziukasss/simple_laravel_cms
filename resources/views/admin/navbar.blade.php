<nav class="light-blue grey darken-3" role="navigation">
    <div class="nav-wrapper">
        <ul class="right hide-on-med-and-down">
            <li><a href="/admin_cms"><span class="material-icons">face</span>Admin</a></li>
            <li><a title="View Website" href="/"><span class="material-icons">language</span>Web</a></li>
            <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    <span class="material-icons">exit_to_app</span>
                    Logout</a></li>
            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                  style="display: none;">{{ csrf_field() }}
            </form>
        </ul>

        <ul id="nav-mobile" class="side-nav">
            <li><a href="/admin_cms"><span class="material-icons">face</span>Admin</a></li>
            <li><a title="View Website" href="/"><span class="material-icons">language</span>Web</a></li>
            <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    <span class="material-icons">exit_to_app</span>Logout</a></li>
            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                  style="display: none;">{{ csrf_field() }}
            </form>
        </ul>
        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
</nav>