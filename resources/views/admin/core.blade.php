<!DOCTYPE html>
<html lang="en">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>CMS-Laravel</title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">

    <link href="{{ URL::asset('css/admin_panel.css') }}" type="text/css" rel="stylesheet" media="screen,projection"/>

</head>
<body>

@include('admin.navbar')
<div class="row">

    @include('admin.sidbar')

    <div class="col s12 m8 l9"> <!-- Note that "m8 l9" was added -->
        @yield('content')

    </div>

    <div class="col s12 m8 l9"> <!-- Note that "m8 l9" was added -->

        Tables
        <!-- Teal page content


              This content will be:
          9-columns-wide on large screens,
          8-columns-wide on medium screens,
          12-columns-wide on small screens  -->
    </div>

</div>


<!--  Scripts-->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script src="/js/admin_panel.js"></script>





</body>


</html>
