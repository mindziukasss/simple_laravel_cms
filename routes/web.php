<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//No need now
//Auth::routes();
//Route::get('/home', 'HomeController@index')->name('home');


Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('login', ['as' => '', 'uses' => 'Auth\LoginController@login']);
Route::get('/admin_cms', 'HomeController@index');
Route::post('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);



Route::post('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);

Route::group(['prefix' => '/admin_cms', 'middleware' => ['auth']], function () {

    Route::get('/', function () {
        return view('admin.core');
    });

    Route::resource('page', 'PageController');

    Route::resource('image', 'GalleryController');


    Route::post('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);

    Route::group(['prefix' => 'category'], function () {
        Route::get('/', 'CategoryController@index')->name('category.index');
        Route::get('/create', 'CategoryController@create')->name('category.create');
        Route::post('/create', 'CategoryController@store');

        Route::group(['prefix' => '{id}'], function () {
            Route::get('/edit', 'CategoryController@edit')->name('category.edit');
            Route::post('/edit', 'CategoryController@update');
            Route::delete('/delete', 'CategoryController@destroy')->name('category.delete');

        });
    });
    Route::resource('subcategory', 'SubCategoryController');
});


